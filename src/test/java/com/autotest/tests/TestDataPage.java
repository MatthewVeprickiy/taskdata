package com.autotest.tests;

import com.autotest.pages.DataPage;
import com.autotest.pages.LoginPage;
import com.autotest.pages.MainPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


@RunWith(Parameterized.class)
public class TestDataPage extends BaseTest {

    String login = "default_login";
    String password = "default_password";

    public TestDataPage(String browser) {
        super(browser);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"chrome"}, //{"firefox"}
        });
    }

    @Test // Задание #2
    public void dataSearchByGroup() {
        String registryName = "Группа ЕИ";

        LoginPage loginPage = new LoginPage(getDriver());
        MainPage mainPage = loginPage.login(login, password);

        assertNotNull("Login failed with " + login + ":" + password, mainPage);

        DataPage dataPage = mainPage.SwitchToDataTab();

        assertTrue(
                "Registry \"" + registryName + "\" not found.",
                dataPage.selectItemOnRegistryComboBox(registryName));

        dataPage.clickOnSearchButton();

        assertTrue(
                "Registry \"" + registryName + "\" is empty.",
                dataPage.checkIfFoundSomething());
    }

    @Test // Задание #1
    public void searchDataTabButton() {
        LoginPage loginPage = new LoginPage(getDriver());
        MainPage mainPage = loginPage.login(login, password);

        assertNotNull("Login failed with " + login + ":" + password, mainPage);

        assertTrue(
                "Search by CSS and XPath returns different elements.",
                mainPage.searchDataButtonByMultipleWays());
    }
}
