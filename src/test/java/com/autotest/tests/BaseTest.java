package com.autotest.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.junit.After;

import static org.junit.Assert.fail;


public class BaseTest {

    private WebDriver driver;

    public BaseTest(String browser) {
        try {
            setDriver(browser);
            driver.manage().window().maximize();
        } catch (Exception e) {
            fail("Ошибка: " + e.getMessage());
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

    private void setDriver(String browser) {
        switch (browser) {
            case "chrome":
                driver = initChromeDriver();
                break;
            case "firefox":
                driver = initFirefoxDriver();
                break;
            default:
                System.out.println("Браузер : " + browser + " не поддерживается. Запуск Google Chrome по-умолчанию.");
                driver = initChromeDriver();
        }
    }

    private WebDriver initChromeDriver() {
        setDriverPath("chromedriver.exe");
        return new ChromeDriver();
    }

    private WebDriver initFirefoxDriver() {
        setDriverPath("geckodriver.exe");
        return new FirefoxDriver();
    }

    private void setDriverPath(String driver) {
        System.setProperty("webdriver.gecko.driver", "C:\\WebDrivers\\" + driver);
    }
}