package com.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/*
 *  Понятие "Главная страница" видимо свелось к левому аккордеону,
 *  где есть переключение между "вкладками" и меню пользователя.
 */
public class MainPage extends BasePage {

    @FindBy(css = ".un-mainmenu-body .un-mainmenu-list-center .un-mainmenu-item-dataprocessinglist .un-mainmenu-list-body .un-mainmenu-item-data")
    private WebElement mainMenuDataButton;

    MainPage(WebDriver driver) {
        super(driver);

        PageFactory.initElements(driver, this);
        waitElementToBeClickable(mainMenuDataButton);
    }

    public DataPage SwitchToDataTab() {
        mainMenuDataButton.click();

        return new DataPage(getDriver());
    }

    /* Решение задания №1. Снова попытается найти кнопку "Данные", но уже с помощью XPath. */
    public boolean searchDataButtonByMultipleWays() {

        WebElement dataButtonByXPath = getElementByXPath(
                "//div[@class='un-mainmenu-body']/div[contains(@class, 'un-mainmenu-list-center')]//div[contains(@class, 'un-mainmenu-item-dataprocessinglist')]//div[@class='un-mainmenu-list-body']/div[contains(@class, 'un-mainmenu-item-data')]");

        return mainMenuDataButton.equals(dataButtonByXPath);
    }
}
