package com.autotest.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage extends BasePage {

    @FindBy(css = ".un-login-form-input-container input[type='text']")
    private WebElement userNameField;

    @FindBy(css = ".un-login-form-input-container input[type='password']")
    private WebElement passwordField;

    @FindBy(css = ".un-login-form-input-container a[id^='button']")
    private WebElement sumbitButton;


    public LoginPage(WebDriver driver) {
        super(driver);

        driver.navigate().to("http://rzds.unidata-platform.com");
        PageFactory.initElements(driver, this);
        waitElementToBeClickable(userNameField);
    }

    public MainPage login(String username, String password) {
        setText(userNameField, username);
        setText(passwordField, password);
        clickOnElement(sumbitButton);

        try {
            // Выглядит как лучший вариант, чем поиск тоста с ошибкой.
            // В нормальном случае быстрее найти кнопку "Данные" в конструкторе MainPage,
            // чем ждать появление тоста с ошибкой.
            return new MainPage(getDriver());
        } catch (TimeoutException e) {
            return null;
        }
    }
}