package com.autotest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


class BasePage {
    private WebDriver driver;
    private int defaultTimeoutInSeconds = 5;

    BasePage(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver getDriver() {
        return driver;
    }

    void setText(WebElement webElement, String text) {
        webElement.clear();
        webElement.sendKeys(text);
    }

    void clickOnElement(WebElement button) {
        waitElementToBeClickable(button);
        button.click();
    }

    WebElement getElementByClass(String className) {
        return getElement(By.className(className));
    }

    WebElement getElementByXPath(String xPath) {
        return getElement(By.xpath(xPath));
    }

    private WebElement getElement(By by) {
        return (new WebDriverWait(driver, defaultTimeoutInSeconds))
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    void waitElementToBeClickable(WebElement webElement) {
        new WebDriverWait(driver, defaultTimeoutInSeconds)
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }
}