package com.autotest.pages;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


/*
 * DataPage в текущем представлении должно управлять меню поиска по данным (реестры и справочники).
 * Панель с результатами при дальнейшем развитии дожна быть реализована в виде другого класса.
 */
public class DataPage extends BasePage {

    @FindBy(css = ".un-section-data .un-query .x-panel-body .un-query-pinned-sections .entity")
    private WebElement registryComboBox;

    @FindBy(css = ".un-section-data .un-query .x-panel-body .un-query-pinned-sections .x-btn .x-btn-button")
    private WebElement searchButton;


    DataPage(WebDriver driver) {
        super(driver);

        PageFactory.initElements(driver, this);
        waitElementToBeClickable(registryComboBox);
    }

    /* Выбор элемента из списка реестров по его названию. */
    public boolean selectItemOnRegistryComboBox(String item) {
        /* Кликать при этом не обязательно. */
        registryComboBox.click();

        try {
            getElementByXPath(
                    "//div[contains(@class, 'x-tree-panel')]//div[contains(@class, 'x-grid-item-container')]//span[text()='" + item + "']")
                    .click();

            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    public void clickOnSearchButton() {
        clickOnElement(searchButton);
    }

    /* Проверяет наличие хотя бы одного элемента в списке результатов. */
    public boolean checkIfFoundSomething() {
        try {
            getElementByXPath(
                    "//div[contains(@class, 'x-panel-un-result')]//div[contains(@class, 'x-grid-item-container')]/table");

            return true;
        } catch (TimeoutException e) {
            return false;
        }

    }
}
